import sugar
import utils
from tokenizer import Token, Tokens

type
  Program* = seq[Symbol]
  Symbol* = string

type
  Parser* = (Tokens) -> Program

func parseProgram*(input: Tokens): Parser =
  func(input: Tokens): Program =
    var program = Program.init()
    program.add("Hello")
    return program

proc hello*() =
  echo "Hello, World!"
  echo repr(@[].parseProgram()(@[]))
