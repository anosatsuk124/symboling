import sugar

type
  Tokens* = seq[Token]
  Token* = object
    kind: TokenKind
    value: string
  TokenKind* = enum
    tkSymbol
    tkPunctuation
    tkComment
    tkWhitespace

type
  Tokenizer* = (input: string) -> seq[Token]
