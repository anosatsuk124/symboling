# Package

version = "0.1.0"
author = "Satsuki Akiba"
description = "An experimental programming language"
license = "GPL-3.0-or-later"
srcDir = "src"
binDir = "target/bin"
installExt = @["nim"]
bin = @["symboling"]

# Compile Options
--gc: orc

# Dependencies

requires "nim >= 2.0.4"
